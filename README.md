# Notes for articles

Hi, I have decided to take notes on articles that I read. This has been in my
plans for a while now and even though I have done this in various other avenues, 
I want to centralized and reorganize my notes into a single git respository.
I will then add this git repository as a submodule in my personal repo.

Feel free to read my notes & also point of your criticisms and expansions to
the same ideas via PRs and Issues!

## Contents

### Science

### Philosophy

### News
